# Sciences Numérique et Technologie

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------


Lien vers le dépôt GitLab des ressources :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/snt/-/blob/master/docs)

Lien vers la page web image du dépôt :

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/snt)
