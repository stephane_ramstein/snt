
## Le web

- Le navigateur web : [pdf](./web/navigateur_web.pdf) ou [odp](./web/navigateur_web.odp)
- Cours sur les URL : [pdf](./web/02_TP_cours_URL.pdf) ou [odt](./web/02_TP_cours_URL.odt)
- Ajouter du style à sa page web : [pdf](./web/delplace_nicolas_tp_css/tp_css.pdf) ou [odt](./web/delplace_nicolas_tp_css/tp_css.odt)
    - page web à modifier : [html](./web/delplace_nicolas_tp_css/index.html)
    - feuille de style à modifier : [css](./web/delplace_nicolas_tp_css/style.css)
    - image de carte MAGIC : [jpg](./web/delplace_nicolas_tp_css/proxy-image.jpeg)
    - page web à reproduire si vous avez terminé le TP : [jpg](./web/delplace_nicolas_tp_css/a_reproduire.png)
    - Aide sur le CSS : [png](./web/delplace_nicolas_tp_css/cheat_sheet_css.png)


<!-- - Une page web avec du style : [pdf](./web/03_TP_css.pdf) ou [odt](./web/03_TP_css.odt)
    - Fichiers pour le TP : [zip](./web/03_TP_css_ressources.zip)
- TP meringues : [pdf](./web/04_TP_meringues.pdf) ou [odt](./web/04_TP_meringues.odt)
    - Fichiers pour le TP : [zip](./web/03_TP_css_ressources.zip)
- Comprendre le PageRank : [pdf](./web/05_PageRank.pdf) ou [odt](./web/05_PageRank.odt)
- DM sur les cookies : [pdf](./web/cookies.pdf) -->


## Objets connectés

- Réalisation d'un objet connecté : [pdf](./systemes_embarques_objets_connectes/02_objet_connecte.pdf) ou [odt](./systemes_embarques_objets_connectes/02_objet_connecte.odt)
    - Module python de l'antenne WIFI : [py](https://stephane_ramstein.gitlab.io/nsi/outils/capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/esp8285.py) (click droit - enregistrer la cible du lien sous)
    - Page web modèle pour le serveur : [html](https://stephane_ramstein.gitlab.io/nsi/outils/capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/) (click droit - enregistrer la cible du lien sous, renommer en index.html)
    - Programme python du serveur : [py](./systemes_embarques_objets_connectes/serveur_microbit.py) (click droit - enregistrer la cible du lien sous)


## La photographie numérique

- Tp-cours sur les images noir et blanc et niveaux de gris : [pdf](./photographie_numerique/Photo_numerique_I.pdf)
- Tp-cours sur les images couleur : [pdf](./photographie_numerique/Photo_numerique_II.pdf)
- Cours sur les formats d'images, leurs algorithmes de traitement et métadonnées : [pdf](./photographie_numerique/Photo_numerique_III_cours.pdf)
    - Video d'introduction : [lien](https://www.youtube.com/watch?v=0PhlxPqtIB0)
    - Image pour le cours : [jpeg](./photographie_numerique/maya.jpeg)


## Python 3/4

- Tp-cours sur la structure itérative : [pdf](./python/python_structure_itérative.pdf) ou [odt](./python/python_structure_itérative.odt)



## Localisation, cartographie et mobilité (Philippe Boddaert)

- Activité sur le principe du GPS : [pdf](./geolocalisation_philippe/LO-C1.pdf)
- Activité sur le calcul d'itinéraires : [pdf](./geolocalisation_philippe/LO-C2.pdf)


## Introduction au langage Python

- Algorea adventure : [Algorea adventure](https://parcours.algorea.org/contents/4707-4702-100575556387408660/)
    - Chapitre 1, affichage et ordre des instructions : [chapitre 1](https://parcours.algorea.org/contents/4707-4702-100575556387408660-1788359139685642917/)
    - Chapitre 2, les boucles bornées : [chapitre 2](https://parcours.algorea.org/contents/4707-4702-100575556387408660-1410985367628944162/)


## Localisation, cartographie et mobilité

- Video d'introduction : [lien](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-183305583351435935-124429449586073261-532830624346231831/)
- Activité sur le principe du GPS : [pdf](./geolocalisation/La_Geolocalisation.pdf)
    - localisation sur Algorea : [lien](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-183305583351435935-1207970506541061357-237778358454750514-380941821572110134/)
    - trilatération sur Algorea : [lien](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-183305583351435935-1207970506541061357-237778358454750514-88752303685492924/)


- Activité sur les cartes et le calcul d'itinéraires : [pdf](./geolocalisation/TP_02_geoportail.pdf) ou [odt](./geolocalisation/TP_02_geoportail.odt)
    - lien vers Géoportail : [lien](https://www.geoportail.gouv.fr/)
    - lien vers l'activité Graphes de Algorea : [lien](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-183305583351435935-124429449586073261-305211257887472178/)
    - lien vers l'activité Itinéraires de Algorea : [lien](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-183305583351435935-124429449586073261-73021242266738836/)

--------------------

[Evaluation de l'investissement et du sérieux](./Notes_serieux/27.pdf)
