from microbit import *
import esp8285

display.scroll('Initialisation', delay = 100, wait=False, loop=True)

esp8285.init('reseau_TP_info', '')
ip_address = esp8285.get_ip()

display.scroll(ip_address + ' pret - presser A', delay = 100, wait=False, loop=True)

while not button_a.is_pressed():
    pass

display.scroll('En attente de requete ...', delay = 100, wait=False, loop=False)

while True:
    page = esp8285.load_page('index.html')
    requete, connection_id = esp8285.get_request()
    display.scroll('GET ' + str(requete), delay = 100, wait=False, loop=False)
    if requete == '/couleur':
        page = page.replace('$variable$', 'bleu')
    else:
        page = page.replace('$variable$', 'requête invalide')
    esp8285.send_data(page, connection_id)
