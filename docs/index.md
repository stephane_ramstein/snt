# Sciences Numérique et Technologie

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------


Lien vers le dépôt GitLab des ressources :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/snt/-/blob/master/docs)

Lien vers la page web image du dépôt :

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/snt)

--------------------


## Consignes de rentrée

La cours s'articule entre travaux pratiques en demi-groupes à raison d'une heure par semaine et cours-activités en classe entière à raison d'une heure tous les quinze jours.

Evaluation:

- Evaluation écrite : sous forme de questions classiques et QCM. Les élèves ont droit à tous leurs documents de cours. Chaque évaluation comporte des questions de l'évaluation précédente ou se rapportant à du travail qui était à faire à la maison.
<!-- - Evaluation orale : dans l'année les élèves auront à intervenir devant la classe sur des sujets divers. -->
- Evaluation pratique : certaines activités pratiques seront évaluées, sous forme de bonus à la moyenne.
- Evaluation de l'investissement et du sérieux : lors de toutes les activités, sous forme de bonus à la moyenne.


## Systèmes embarqués

- Video d'introduction : [lien](https://www.youtube.com/watch?v=DOECi_ZKaYI)
- Activité d'étude du principe d'un système embarqué à base de microcontrôleur : [pdf](./systemes_embarques_objets_connectes/01_microcontroleur.pdf) ou [odt](./systemes_embarques_objets_connectes/01_microcontroleur.odt)
    - Fiche détaillant le microcontrôleur Micro:bit : [pdf](./systemes_embarques_objets_connectes/109_Carte_MicroBit.pdf) ou [odt](./systemes_embarques_objets_connectes/109_Carte_MicroBit.odt)
    - Editeur Python en ligne pour microcontrôleur Micro:bit : [lien](https://python.microbit.org)
- DM sur le freinage ABS : [pdf](./systemes_embarques_objets_connectes/freinage_ABS.pdf) ou [odt](./systemes_embarques_objets_connectes/freinage_ABS.odt)
- Réalisation d'un système embarqué : [pdf](./systemes_embarques_objets_connectes/un_tamagotchi.pdf) ou [odt](./systemes_embarques_objets_connectes/un_tamagotchi.odt)
- Capteurs, actionneurs, communication et systèmes embarqués pour la carte micro:bit : [lien](https://stephane_ramstein.gitlab.io/nsi/outils/capteurs_actionneurs/)


## Python 1/4

- Tp-cours sur les entrées-sorties clavier, les variables et le type des données : [pdf](./python/python_affectation_entrees_sorties.pdf) ou [odt](./python/python_affectation_entrees_sorties.odt)
<!-- - Qu'est-ce qu'une page web : [pdf](./web/01_TP_page_web.pdf) ou [odt](./web/01_TP_page_web.odt) -->


## Le web

- Qu'est-ce qu'une page web : [pdf](./web/TP_page_web_vanoverberghe.pdf) ou [odt](./web/TP_page_web_vanoverberghe.odt)
- Cours sur les URL : [pdf](./web/02_TP_cours_URL.pdf) ou [odt](./web/02_TP_cours_URL.odt)
- Construire une page HTML : [pdf](./web/TP_html_Seconde_boulier.pdf) ou [odt](./web/TP_html_Seconde_boulier.odt)
- Ajouter du style à une page HTML : [pdf](./web/TP_CSS_vanoverberghe.pdf) ou [odt](./web/TP_CSS_vanoverberghe.odt)
    - Page web à réaliser en fin de TP : à ajouter
- Comprendre le PageRank : [pdf](./web/05_PageRank.pdf) ou [odt](./web/05_PageRank.odt)


## Python 2/4

- Tp-cours sur la structure conditionnelle : [pdf](./python/python_structure_conditionnelle.pdf) ou [odt](./python/python_structure_conditionnelle.odt)


<!-- [Evaluation de l'investissement et du sérieux](./Notes_serieux/27.pdf) -->


## Les données structurées et leur traitement

- Activité d'introduction d'algorea : [pdf](./donnees/TP_01_introduction.pdf) ou [odt](./donnees/TP_01_introduction.odt)
    - lien pour l'activité : [algorea](https://parcours.algorea.org/fr/a/snt-donnees)
- Quizz sur la video d'introduction : [pdf](./donnees/quizz_introduction.pdf) ou [odt](./donnees/quizz_introduction.odt)
<!-- - Activité sur le format des données : [pdf](./donnees/TP_02_formats_donnees.pdf) ou [odt](./donnees/TP_02_formats_donnees.odt)
    - lien pour l'activité : [algorea](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-169388080788275719/)
- Activité sur le croisement de données : [pdf](./donnees/TP_03_groupes_sanguins.pdf) ou [odt](./donnees/TP_03_groupes_sanguins.odt)
- Activité sur l'utilisation d'un tableur : [pdf](./donnees/TP_03_tableur.pdf) ou [odt](./donnees/TP_03_tableur.odt)
    - lien pour l'activité : [algorea](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-144438975691201992/)
    - données pour l'activité musique : [csv](./donnees/musiques.csv)
    - données pour l'activité Union Européenne : [csv](./donnees/etats_union_europe.csv)
- DM sur le cloud : [pdf](./donnees/dm_cloud.pdf) -->